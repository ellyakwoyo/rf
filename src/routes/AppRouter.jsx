import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

const Navbar = lazy(() => import("../components/Navbar"));

const Home = lazy(() => import("../components/Home"));
const About = lazy(() => import("../components/About"));
const Activities = lazy(() => import("../components/Activities"));
const Download = lazy(() => import("../components/Download"));
const ContactUs = lazy(() => import("../components/ContactUs"));

function AppRouter() {
  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/activities" element={<Activities />} />
          <Route path="/download" element={<Download />} />
          <Route path="/contact-us" element={<ContactUs />} />
        </Routes>
      </Suspense>
    </Router>
  );
}

export default AppRouter;
