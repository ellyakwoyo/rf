
import Layout from './components/Layout.jsx';


function App() {

  return (
    <main id="fixed-wrapper">
        <div>
          <Layout />
        </div>
    </main>
  );
}

export default App;

