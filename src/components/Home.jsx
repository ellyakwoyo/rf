function Home() {
    return (
      <>
		<div className="clearfix"></div>
		<section id="home">
			<div className="home-fixed">
				<div className="container">
					<div className="row">
						<div className="col-md-6">
							<h1>One touch for <span>all needs. Lets talk Agriculture</span></h1>
							<p>
								Join us. Lets walk this journey together.
							</p>
							<p className="btn-horizontal hovering">
								<a href="#" 
                                    className="btn bg-light btn-lg rounded-pill d-flex justify-content-center align-items-center">
										<strong>Get the App</strong>
                                </a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div className="clearfix"></div>
      </>
    )
  }
  
  export default Home