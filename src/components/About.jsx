function About() {
  return (
    <>
      <section id="about" className="white-section">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="heading">
                <h4 className="text-desc">Description</h4>
                <p>Ahero Farmers, United for a purpose</p>
              </div>
              <p>
                Our group is a dynamic community that leverages the passion and
                energy of young individuals to revolutionize farming practices.
                We are committed to fostering innovation and sustainability in
                agriculture, and we believe in the power of youth to drive
                positive change. With a focus on modern techniques and
                responsible farming, we aim to secure a prosperous and
                eco-friendly future for the agricultural industry. Our diverse
                activities span from rice farming and horticulture to
                comprehensive market research and cutting-edge agricultural
                research. Join us as we cultivate the next generation of farmers
                and pave the way for a more sustainable and productive
                agriculture sector.
              </p>
              <p className="btn-horizontal">
                <a
                  href="#"
                  className="btn btn-bordered border 
                  custom-border-color discovering btn-lg rounded-pill 
                  d-flex justify-content-center align-items-center"
                >
                  <strong>Discover</strong>
                </a>
              </p>
            </div>
          </div>
        </div>
      </section>
      <div className="clearfix"></div>
    </>
  );
}

export default About;
