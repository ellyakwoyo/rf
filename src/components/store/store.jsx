import { createStore } from 'redux';

const initialState = {
  currentSlide: 0,
};

const sliderReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'NEXT_SLIDE':
      return {
        ...state,
        currentSlide: (state.currentSlide + 1) % action.payload.length,
      };
    default:
      return state;
  }
};

const store = createStore(sliderReducer);

export default store;
