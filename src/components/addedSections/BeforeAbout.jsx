import phone from "/img/phone.png";
import React, { useState, useEffect } from "react";
import { Carousel } from 'react-bootstrap';


function BeforeAbout({ images, interval }) {
  const [currentSlide, setCurrentSlide] = useState(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentSlide((prevSlide) => (prevSlide + 1) % images.length);
    }, interval);

    return () => {
      clearInterval(intervalId);
    };
  }, [images, interval]);

  return (
    <div className="fixed-phone">
      <div className="fixed-slider">
        <Carousel className="">
          {images.map((image, index) => (
            <Carousel.Item key={index}>
              <img className="d-block w-100" src={image.src} alt={image.alt} />
            </Carousel.Item>
          ))}
        </Carousel>
      </div>
      <img src={phone} className="img-responsive" alt="" />
    </div>
  );
}

export default BeforeAbout;

