// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faEnvelope, faPhone } from "@fortawesome/free-brands-svg-icons";

function ContactUs() {
  return (
    <>
      <div className="clearfix"></div>
      <section id="contact-us" className="containt">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="heading">
                <h4 className="text-success">Contact us</h4>
                <p>Get in touch with us</p>
              </div>
              <ul className="contact-info">
                <li>
                  {/* <FontAwesomeIcon icon={faEnvelope} /> */}
                  <span>aherofarmers.com</span>
                </li>
                <li>
                  {/* <FontAwesomeIcon icon={faPhone} /> */}
                  <span>+254-712-345-678</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default ContactUs;
