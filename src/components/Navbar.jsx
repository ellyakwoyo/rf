
import React, { useState, useEffect } from 'react';

function Navbar() {
  const [activeSection, setActiveSection] = useState('home');

  useEffect(() => {

    const handleScroll = () => {
      const sections = document.querySelectorAll('section');
      sections.forEach((section) => {
        const sectionTop = section.offsetTop;
        const sectionHeight = section.clientHeight;

        if (window.scrollY >= sectionTop - sectionHeight / 2) {
          setActiveSection(section.id);
        }
      });
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <>
      <header>
        <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light">
          <div className="container">
            <a className="navbar-brand" href="#">
              <strong className="text-success">Ahero</strong>Farmers
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarText"
              aria-controls="navbarText"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
              <ul className="navbar-nav mb-2 mb-lg-0 ms-auto">
                <li className="nav-item">
                  <a href="#home" className={`nav-link ${activeSection === 'home' ? 'active' : ''}`}>
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a href="#about" className={`nav-link ${activeSection === 'about' ? 'active' : ''}`}>
                    About
                  </a>
                </li>
                <li className="nav-item">
                  <a href="#features" className={`nav-link ${activeSection === 'features' ? 'active' : ''}`}>
                    Activities
                  </a>
                </li>
                <li className="nav-item">
                  <a href="#download" className={`nav-link ${activeSection === 'download' ? 'active' : ''}`}>
                    Download
                  </a>
                </li>
                <li className="nav-item">
                  <a href="#contact-us" className={`nav-link ${activeSection === 'contact-us' ? 'active' : ''}`}>
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    </>
  );
}

export default Navbar;
