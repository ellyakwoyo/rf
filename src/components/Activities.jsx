function Activities() {
  return (
    <>
      <section id="features" className="primary-section primary">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="heading">
                <h4>Activities</h4>
                <p>Amazing farming activities</p>
              </div>
              <ul className="feature-list margintop40">
                <li>
                  <i className="pe-7s-sun pe-feature pe-3x alignleft"></i>
                  <h4>Rice Farming</h4>
                  <p>
                    Explore our sustainable rice farming practices, from
                    planting to harvest. Join us in nurturing and cultivating
                    the future of agriculture.
                  </p>
                </li>
                <li>
                  <i className="pe-7s-edit pe-feature pe-3x alignleft"></i>
                  <h4>Horticulture</h4>
                  <p>
                    Discover the art of horticulture with our diverse range of
                    fruits, vegetables, and ornamental plants. Grow with us,
                    naturally.
                  </p>
                </li>
                <li>
                  <i className="pe-7s-tools pe-feature pe-3x alignleft"></i>
                  <h4>Agricultural research</h4>
                  <p>
                    Empowering agriculture through cutting-edge research.
                    Explore innovations, practices, and solutions for
                    sustainable and thriving agribusiness.
                  </p>
                </li>
                <li>
                  <i className="pe-7s-portfolio pe-feature pe-3x alignleft"></i>
                  <h4>Market reasearch</h4>
                  <p>
                    Gain agricultural insights through our market research.
                    Explore trends, data, and strategies for informed
                    agribusiness growth and success.
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div className="clearfix"></div>
    </>
  );
}
export default Activities;
