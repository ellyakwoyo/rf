import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faTwitter } from "@fortawesome/free-brands-svg-icons";
import React, { useState } from "react";

function Footer() {
  const [email, setEmail] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Email submitted:", email);
  };
  return (
    <>
      <footer className="primary">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="widget">
                <h4>Email Newsletters</h4>
                <form>
                  <form
                    className="subscribe-form marginbot20"
                    onSubmit={handleSubmit}
                  >
                    <input
                      className="subscribe-input"
                      type="email"
                      placeholder="Enter your email address"
                      value={email}
                      onChange={handleEmailChange}
                      required
                    />
                    <button className="subscribe-btn" type="submit">
                      GO
                    </button>
                  </form>
                </form>
                <p>2023 &copy; Copyright Ahero Farmers</p>
              </div>
            </div>
            <div className="col-md-2">
              <div className="widget">
                <h4>Follow us</h4>
                <div className="social-network">
                  <a href="#">
                    {" "}
                    <FontAwesomeIcon icon={faFacebook} /> Facebook
                  </a>
                  <a href="#">
                    {" "}
                    <FontAwesomeIcon icon={faTwitter} /> Twitter
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;
