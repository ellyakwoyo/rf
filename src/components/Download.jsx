function Download() {
  return (
    <section id="download" className="fixed-containt">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="heading">
              <h4>Join us</h4>
              <p>Get app now !</p>
            </div>
            <p>
              Download our app now and join the agricultural revolution. Farm
              smarter, grow better, and make a difference in agriculture.
            </p>
            <p className="btn-horizontal hovering">
              <a
                href="#"
                className="btn bg-light btn-lg rounded-pill d-flex justify-content-center align-items-center"
              >
                <strong>Get the App</strong>
              </a>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
export default Download;
