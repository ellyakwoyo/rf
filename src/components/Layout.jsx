import AppRouter from "../routes/AppRouter.jsx"
import Home from "./Home.jsx";
import ContactUs from "./ContactUs.jsx";
import About from "./About.jsx";
import Footer from "./Footer.jsx";
import Activities from "./Activities.jsx";
import Navbar from "./Navbar.jsx"
import Download from "./Download.jsx";
import BeforeAbout from "./addedSections/BeforeAbout.jsx";

function Layout() {
  // const images = [
  //   {
  //     src: "img/app/app1.png",
  //     alt: "Slide 1",
  //   },
  //   {
  //     src: "img/app/app2.png",
  //     alt: "Slide 2",
  //   },
  //   {
  //     src: "img/app/app3.png",
  //     alt: "Slide 3",
  //   },
  //   {
  //     src: "img/app/app4.png",
  //     alt: "Slide 4",
  //   },
  // ];
  return (
    <>
      <div>
          <Navbar />
          <Home />
          {/* <BeforeAbout images={images} interval={3000} /> */}
          <BeforeAbout />
          <About />
          <Activities />
          <Download />
          <ContactUs />
          <Footer />
      </div>
    </>
  );
}
export default Layout;
